Source: bioawk
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
Build-Depends: debhelper-compat (= 13),
               bison,
               libz-dev
Standards-Version: 4.5.1
Homepage: https://github.com/lh3/bioawk
Vcs-Browser: https://salsa.debian.org/med-team/bioawk
Vcs-Git: https://salsa.debian.org/med-team/bioawk.git
Rules-Requires-Root: no

Package: bioawk
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: extension of awk for biological sequence analysis
 Bioawk is an extension to Brian Kernighan's awk, adding the support of
 several common biological data formats, including optionally gzip'ed BED, GFF,
 SAM, VCF, FASTA/Q and TAB-delimited formats with column names. It also adds a
 few built-in functions and an command line option to use TAB as the
 input/output delimiter. When the new functionality is not used, bioawk is
 intended to behave exactly the same as the original BWK awk.
